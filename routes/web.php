<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// HOMEPAGE
Route::get('/', 'PagesController@getHome');
// END HOMEPAGE ----------------------------------------------

// ABOUT PAGE
Route::get('/about', 'PagesController@getAbout');
// END ABOUT PAGE---------------------------------------------

// CONTACT PAGE
Route::get('/contact', 'PagesController@getContact');
Route::post('/contact/submit', 'MessagesController@submit');
// MESSAGES
Route::get('/messages', 'MessagesController@getMessages');
// END CONTACT------------------------------------------------

// WORK PAGE
Route::get('/work', 'PagesController@getWork');
Route::get('work.js', 'PagesController@getJs');
Route::get('work.css', 'PagesController@getCss');
Route::get('work.php', 'PagesController@getPhp');


// END WORK---------------------------------------------------

// BLOG PAGE
Route::get('/blog', 'PagesController@getBlog');
// END BLOG----------------------------------------------------
