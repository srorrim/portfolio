<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getHome(){
      return view('/home');
    }
    public function getAbout(){
      return view('/about');
    }
    public function getContact(){
      return view('/contact');
    }
    public function getWork(){
      return view('/work');
    }
    public function getJs(){
      return view('work.js');
    }
    public function getPhp(){
      return view('work.php');
    }
    public function getCss(){
      return view('work.css');
    }
    public function getBlog(){
      return view('/blog');
    }
}
