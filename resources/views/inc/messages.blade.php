<div id="contact-form">
@if(count($errors) > 0)
  @foreach($errors->all() as $error)
      <div class='alert alert-danger fade-in four' id='status'>
        {{$error}}
      </div>
    @endforeach
  @endif
    @if(session('success'))
      <div class='alert alert-warning fade-in four' id='status'">
        {{session('success')}}
      </div>
    @endif
</div>
