<div class="navbar" id="topNav">
  <a href="/" class="active logo" id="logo" title="logo"></a>
  <div class="navoptions">
    <a href="/contact"><i class="far fa-envelope"></i></a>
    <a href="/blog"><i class="far fa-comment"></i></a>
    <a href="/about"><i class="far fa-address-card"></i></a>
    <a href="/work"><i class="far fa-folder-open"></i></a>
    </div>
    <a href="javascript:void(0);" class="icon" onclick="navbarOpen()">&#9776;</a>
</div>
<script>
function navbarOpen() {
    var x = document.getElementById("topNav");
    if (x.className === "navbar") {
        x.className += " responsive";
        } else {
            x.className = "navbar";
        }
    }
</script>
