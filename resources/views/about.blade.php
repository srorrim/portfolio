@extends('layouts.app')
@section('content')


    <div class="row">

      <div class="about-pic fade-in two" title="Mackenzie Langen - Profile Pic"></div>

      <div class="about-text">
        <h3 class="fade-in two"><strong>My name is ...<h1>Mack Langen,</h1></strong><br>I'm a front-end web developer from Calgary, Alberta.</h3>

        <h2 class="fade-in three">Just trying to make the web an even crazier place.</h2>
        <hr>


        <div class="fade-in four">
        <h2><strong>Some of my skills include:</strong></h2>

        <div class="btn btn-outline-secondary about">SEO</div>
        <div class="btn btn-outline-secondary about">Javascript</div>
          <div class="btn btn-outline-secondary about">HTML5 / CSS3 / Sass</div><br>
          <div class="btn btn-outline-secondary about">php / Laravel</div>
          <div class="btn btn-outline-secondary about">MySQL</div>
          <div class="btn btn-outline-secondary about">jQuery</div>
          <div class="btn btn-outline-secondary about">Google Adwords</div>
          <br><br>
          <hr>
          <h2><strong>Technologies I'm interested in:</strong></h2>

          <div class="btn btn-outline-secondary about">Machine Learning</div>
          <div class="btn btn-outline-secondary about">React / Angular / Vue</div>
            <div class="btn btn-outline-secondary about">Meteor</div><br>
            <div class="btn btn-outline-secondary about">express.js</div>
            <div class="btn btn-outline-secondary about">ASP.NET core</div>
            <div class="btn btn-outline-secondary about">Docker</div>
            <div class="btn btn-outline-secondary about">AR / VR</div>
        </div>
        <hr>
        <h4 class="fade-in four">Currently taking on new projects; If you would like to work together,<br>
          <a href="mailto:macklangen.creative@gmail.com"><strong>please send me an email!</strong></h4></a>
        </div>

    </div>


@endsection
