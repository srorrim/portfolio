@extends('layouts.app')
@section('content')
<div class="contact-container">
    <div class="hero-section-text fade-in one">
      <h1>Contact</h1>
      <h5>Send me a message, I love a challenge.</h5><br>
      <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/RESUME.pdf" download>
        <button class="btn btn-outline-secondary fade-in two">Resume</button></a>
        <a href="https://www.linkedin.com/in/macklangen" download>
          <button class="btn btn-outline-secondary fade-in two">LinkedIn</button></a>

    </div>
  <div id='contact-form'>
    {!! Form::open(['url' => 'contact/submit', 'class' => 'contact-form-handler']) !!}
        <div class="form-group fade-in one">
          {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name...']) }}<br/>
          {{ Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Enter Email...']) }}<br/>
          {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Enter Message...']) }}
        </div>
        <div id="submit-row">
          {{ Form::submit('Send', ['class' => 'btn btn-secondary fade-in two']) }}
        </div>
    {!! Form::close() !!}
  </br>
  </div>
  <iframe width="150" height="50" frameborder="0" style="border:0"
  src="https://www.google.com/maps/embed/v1/place?q=Calgary&key=AIzaSyC-jx7NikO74q9utZuT-eU7_fJpU6Tpw5M" allowfullscreen></iframe>
</div>
  @endsection
