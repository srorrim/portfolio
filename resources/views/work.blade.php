@extends('layouts.app')
@section('content')
  <div class="hero-section">
    <div class="hero-section-text">
      <h1>Projects</h1>
    </div>
  </div>

<div id="work-section">

  <div class="row">

  <div class="work card fade-in one">
    <div class="wrapper" id="work1" title="Music Nightclub">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-js-square"></i>
            <i class="fas fa-chevron-down fa-xs chev"></i></h1>
          <p class="text">A makeshift MIDI controller that was made with the help of Javascript!</p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/KeyboardInstruments/js_drumkit/Js_Drumkit.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in two">
    <div class="wrapper" id ="work2" title="Text-to-voice Machine">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-js-square"></i>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">This Text-to-Voice Machine was as fun to build as it is to use!</p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/text-to-voice/Voice_to_text/index.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in three">
    <div class="wrapper" id="work3" title="Srorrim - Blog Site">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-php"></i>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">The makings of a blog site i have been using as a php testing ground</p>
          <a href="http://srorrim.srorrim.com/" class="btn btn-secondary">Visit</a>
        </div>
      </div>
    </div>
  </div>


  <div class="work card fade-in three">
    <div class="wrapper" id="work4" title="CSS Variables Editor">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-css3"></i>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">This simple editor is made to change CSS variables real-time.</p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/CSSVariables/index.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in three">
    <div class="wrapper" id="work7" title="Alien Smashing Game">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-js-square"></i>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">Aliens. . arent they just smashing?</p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/ALIENSMASH/index.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in three">
    <div class="wrapper" id="workR" title="Simple Analog Clock">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-js-square"></i></a>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">Super simple analog clock with Js </p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/Clock/index.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in three">
    <div class="wrapper" id="work6" title="Predicitve Text using Javascript">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><i class="fab fa-js-square"></i>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">This little app uses Js to predict data entries from a JSON file.</p>
          <a href="https://s3.ca-central-1.amazonaws.com/portfolio.bucket/Javascript/predictiveText/predictionAjax/index.html" class="btn btn-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  {{-- <div class="work card fade-in three">
    <div class="wrapper" id="work3">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><a href="work.css"><i class="fab fa-css3"></i></a>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">Here's some of the Styles I Like to use.</p>
          <a href="work.css" class="btn btn-outline-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="work card fade-in three">
    <div class="wrapper" id="work3">
      <div class="header">
      </div>
      <div class="data">
        <div class="content">
          <h1 class="title"><a href="work.css"><i class="fab fa-css3"></i></a>
          <i class="fas fa-chevron-down fa-xs chev"></i></h1></h1>
          <p class="text">Here's some of the Styles I Like to use.</p>
          <a href="work.css" class="btn btn-outline-secondary">Demo</a>
        </div>
      </div>
    </div>
  </div> --}}


 </div>
</div>
@endsection
