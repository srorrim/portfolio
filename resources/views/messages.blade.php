@extends('layouts.app')
@section('content')
<br><h1 class="messages">Mack's Messages</h1>

@if(count($messages) > 0)
  @foreach($messages as $message)
    <ul class="list-group messages">
      <li class="list-group-item">Name: {{ $message->name }}</li>
      <li class="list-group-item">Email: {{ $message->email }}</li>
      <li class="list-group-item">Message: {{ $message->message }}</li>
    </ul><br>
  @endforeach
@endif
@endsection
