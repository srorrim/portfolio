@extends('layouts.app')
@section('content')
<div class="panels">

  <div class="panel panel1 fade-in five">
    <p><a href="/work">Code</a></p>
  </div>

  <div class="panel panel4 fade-in two" title="10 Swords"><a href="/about"></a></div>

  <div class="panel panel5 fade-in five">
    <p><a href="/blog">Concept</a></p>
  </div>
</div>

<script>
  const panels = document.querySelectorAll('.panel');

  function toggleOpen() {
    console.log('hello');
    this.classList.toggle('open');
  }

  function toggleActive() {
    if (e.propertyName.includes('flex')) {
      this.classList.toggle('open-active');
    }
  }

  panels.forEach(panel => panel.addEventListener('mouseover', toggleOpen));
  panels.forEach(panel => panel.addEventListener('mouseout', toggleOpen));
  panels.forEach(panel => panel.addEventListener('transitionend', toggleActive));
</script>
@endsection
