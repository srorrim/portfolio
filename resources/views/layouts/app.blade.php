<!DOCTYPE HTML>
<html>
  <head>
     <link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAA
      AAAAAAAAAAAA////AKenpwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARARAAAAAAABEAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAD+PwAA/B8AAMQHAACAAwAAAAEAAAABAAAAAAAAAAAA
      AAAAAAAAAAAAgAEAAIADAACANwAAgC8AAAAvAAD4HwAA" rel="icon" type="image/x-icon" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portfolio of Mackenize Langen - Web Developer">
    <title>Mackenzie Langen - Portfolio</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>


  </head>
  <body>
    @include('inc.navbar')
        @yield('content')
        @include('inc.messages')
      </div>
  </body>

</html>
